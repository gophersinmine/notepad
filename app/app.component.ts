import {Component, ElementRef} from '@angular/core';


import { NoteFormComponent } from './note-form/note-form'
import { InputForm} from './input-form/input-form'
import { NoteService } from './shared/note-service'
import { SEMANTIC_COMPONENTS, SEMANTIC_DIRECTIVES } from "ng-semantic";


@Component({
    selector: 'todo-app',
    templateUrl: './app/app.component.html',
    styleUrls: ['./app/app.component.css'],
    directives: [ NoteFormComponent,InputForm,SEMANTIC_COMPONENTS, SEMANTIC_DIRECTIVES],
    providers: [NoteService]
})

export class AppComponent{
    title:  string;
    day:    string;
    typeOfNotes: string;

    constructor(private elementRef:ElementRef) {
        this.title = 'Taskboard';
        this.day = "Monday";
        this.typeOfNotes = "";
    }

    ngAfterViewInit(){
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.src = '/app/swipe.js';
        this.elementRef.nativeElement.appendChild(s);
    }

    changeTypeOfNotes() {
        if (this.typeOfNotes == ''){
            this.typeOfNotes = 'checked';
        } else {
            this.typeOfNotes = '';
        }
    }

}