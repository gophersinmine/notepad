import { Injectable } from '@angular/core';

import {INote} from './note-model'
import {notes} from './note-date'

@Injectable()
export class NoteService{

    addNote(note: INote): void{
        notes.push(note);
    }

    deleteNote(note: INote):void{
        let index = notes.indexOf(note);
        if (index > -1){
            notes.splice(index,1);
        }
    }
}