export interface INote{
    title:  string;
    day:    string;
    done:   string;
}

export class Note implements INote{
    title:  string;
    day:    string;
    done:   string;

    constructor(title:string,day:string){
        this.title = title;
        this.day = day;
        this.done = '';
    }
}