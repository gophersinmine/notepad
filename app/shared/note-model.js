"use strict";
var Note = (function () {
    function Note(title, day) {
        this.title = title;
        this.day = day;
        this.done = '';
    }
    return Note;
}());
exports.Note = Note;
//# sourceMappingURL=note-model.js.map