"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var note_service_1 = require('../shared/note-service');
var note_model_1 = require('../shared/note-model');
var note_date_1 = require('../shared/note-date');
var NoteFormComponent = (function () {
    function NoteFormComponent(noteService) {
        this.noteService = noteService;
        this.notes = note_date_1.notes;
    }
    NoteFormComponent.prototype.delete = function (note) {
        this.noteService.deleteNote(note);
    };
    NoteFormComponent.prototype.toggleDone = function (note) {
        if (note.done == '') {
            note.done = 'checked';
        }
        else {
            note.done = '';
        }
    };
    Object.defineProperty(NoteFormComponent.prototype, "sortedNotes", {
        get: function () {
            return this.notes
                .map(function (note) { return note; })
                .sort(function (a, b) {
                if (a.title > b.title)
                    return 1;
                else if (a.title < b.title)
                    return -1;
                else
                    return 0;
            })
                .sort(function (a, b) {
                if (a.done && !b.done)
                    return 1;
                else if (!a.done && b.done)
                    return -1;
                else
                    return 0;
            });
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], NoteFormComponent.prototype, "day", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', note_model_1.Note)
    ], NoteFormComponent.prototype, "note", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], NoteFormComponent.prototype, "typeOfNotes", void 0);
    NoteFormComponent = __decorate([
        core_1.Component({
            selector: 'note-form',
            templateUrl: './app/note-form/note-form.html',
            styleUrls: ['./app/note-form/note-form.css']
        }), 
        __metadata('design:paramtypes', [note_service_1.NoteService])
    ], NoteFormComponent);
    return NoteFormComponent;
}());
exports.NoteFormComponent = NoteFormComponent;
//# sourceMappingURL=note-form.js.map