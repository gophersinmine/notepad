import  { Component,Input} from '@angular/core';

import { NoteService } from '../shared/note-service'
import {INote, Note} from '../shared/note-model'
import { notes } from '../shared/note-date'

@Component({
    selector: 'note-form',
    templateUrl: './app/note-form/note-form.html',
    styleUrls: ['./app/note-form/note-form.css']
})

export class NoteFormComponent {
    @Input() day: string;
    @Input() note: Note;
    @Input() typeOfNotes: string;

    notes: INote[];
    constructor(private noteService:NoteService){
        this.notes = notes;
    }

    delete(note: any){
        this.noteService.deleteNote(note);
    }

    toggleDone(note:any) {
        if (note.done == ''){
            note.done = 'checked';
        } else {
            note.done = '';
        }
    }

    get sortedNotes(): INote[] {
        return this.notes
            .map(note => note)
            .sort((a, b) => {
                if (a.title > b.title) return 1;
                else if (a.title < b.title) return -1;
                else return 0;
            })
            .sort((a, b) => {
                if (a.done && !b.done) return 1;
                else if (!a.done && b.done) return -1;
                else return 0;
            });
    }

}
