var myElement = document.getElementById('leftMenu')
var mc = new Hammer(myElement);

mc.on("swiperight ", function() {
    $('.ui.sidebar')
        .sidebar({
            transition:'left',
            closable: false
        })
        .sidebar('show');
});

mc.on('swipeleft',function () {
   $('.ui.sidebar')
       .sidebar({
           transition:'right',
           closable: false
       })
       .sidebar('show');
});

function closeMenu() {
    $('.ui.sidebar')
        .sidebar('hide');
};