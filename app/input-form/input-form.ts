import {Component,Input} from '@angular/core';
import { SEMANTIC_COMPONENTS, SEMANTIC_DIRECTIVES } from "ng-semantic";

import { Note } from '../shared/note-model';
import { NoteService } from '../shared/note-service';


@Component({
    selector: 'input-form',
    templateUrl: './app/input-form/input-form.html',
    styleUrls: ['./app/input-form/input-form.css'],
    directives:[SEMANTIC_COMPONENTS, SEMANTIC_DIRECTIVES]

})

export class InputForm {
    @Input() day:string;
    formIsEmpty: boolean;

    constructor(private noteService: NoteService){}


    add(title:string){
        if (title){
            this.formIsEmpty = false;
            let note = new Note(title,this.day);
            this.noteService.addNote(note);
        } else {
            this.formIsEmpty = true;
        }
    }


}