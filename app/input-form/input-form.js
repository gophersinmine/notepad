"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ng_semantic_1 = require("ng-semantic");
var note_model_1 = require('../shared/note-model');
var note_service_1 = require('../shared/note-service');
var InputForm = (function () {
    function InputForm(noteService) {
        this.noteService = noteService;
    }
    InputForm.prototype.add = function (title) {
        if (title) {
            this.formIsEmpty = false;
            var note = new note_model_1.Note(title, this.day);
            this.noteService.addNote(note);
        }
        else {
            this.formIsEmpty = true;
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], InputForm.prototype, "day", void 0);
    InputForm = __decorate([
        core_1.Component({
            selector: 'input-form',
            templateUrl: './app/input-form/input-form.html',
            styleUrls: ['./app/input-form/input-form.css'],
            directives: [ng_semantic_1.SEMANTIC_COMPONENTS, ng_semantic_1.SEMANTIC_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [note_service_1.NoteService])
    ], InputForm);
    return InputForm;
}());
exports.InputForm = InputForm;
//# sourceMappingURL=input-form.js.map